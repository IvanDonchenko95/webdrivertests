# language: en
Feature: Registration form
Background:
  Given I open Browser
  
  @reg @all @smoke
  Scenario: registration with valid data
    When I go to regPage
    And I enter valid user name
    And I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    And I close browser

  @reg @all @smoke
  Scenario: registration with empty fields
    When I go to regPage
    And I enter valid user name
    And I click signUp button
    Then I see warning massage
    And I close browser

  @reg @all @smoke
  Scenario: registration with already registered userName
    When I go to regPage
    And I enter already registered user name
    And I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser



  @reg @all @smoke
  Scenario: registration with empty userName field
    When I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with 4 symbols in userName
    When I enter four symbols in user name field
    And I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with 21 symbols in userName
    When I enter twenty_one symbols in user name field
    And I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with spaces in userName
    When I enter spaces in user name field
    And I enter valid email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with empty Email field
    When I enter valid user name
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration without @ in Email
    When I enter valid user name
    And I enter email without @ in email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser



  @reg @all @smoke
  Scenario: registration with 256 symbols in Email
    When I enter valid user name
    And I enter max symbols email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser

  @reg @all @smoke
  Scenario: registration with already registered Email
    When I enter valid user name
    And I enter same email
    And I enter valid password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser

  @reg @all @smoke
  Scenario: registration with empty Password field
    When I enter valid user name
    And I enter valid email
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with 7 symbols in Password
    When I enter valid user name
    And I enter valid email
    And I enter seven symbols password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with 21 symbols in Password
    When I enter valid user name
    And I enter valid email
    And I enter twenty_one symbols password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with spaces symbols in Password
    When I enter valid user name
    And I enter valid email
    And I enter nine spaces symbols in password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with lowercase in Password
    When I enter valid user name
    And I enter valid email
    And I enter lowercase symbols in password
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration without special symbols in Password
    When I enter valid user name
    And I enter valid email
    And I enter password without special symbols
    And I enter confirm password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: registration with empty Confirm Password field
    When I enter valid user name
    And I enter valid email
    And I enter valid password
    And I click signUp button
    Then I see warning massage
    And I close browser


  @reg @all @smoke
  Scenario: redirect to authorization page
    When I click on Sign in link
    Then I enter go to auth page
    And I close browser