package Pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Driver {

    private static ChromeDriver driver = null;

    private Driver() {
    }

    public static WebDriver getInstance() {
        if (driver == null) {
            driver = new ChromeDriver();
        }
        driver.manage().window().maximize();
        return driver;
    }

    public static void close() {
        driver.quit();
        driver = null;
    }
}
//    public static void open(String URL) {
//        driver.get(URL);
//    }