package Pages;

import lombok.Getter;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import java.util.Random;
@Getter
public class RegPage {
    public RegPage() {
        PageFactory.initElements(Driver.getInstance(),this);
    }

    @FindBy(css = "#inputLogin")
    private WebElement userName;
    @FindBy (css = "#inputEmail")
    private WebElement email;
    @FindBy (css = "#inputPass")
    private WebElement password;
    @FindBy (css = "#inputPassAg")
    private WebElement confirmPass;
    @FindBy (css = "#btnSignUp")
    private WebElement signUpButton;
    @FindBy (css = "#backAuth")
    private WebElement signInButton;
    @FindBy (css = "#btnSignGoogle")
    private WebElement singGoogleButton;
    @FindBy (css = "#errorDescription")
    private WebElement errorMassage;

    public void validUserName () {
        userName.click();
        userName.sendKeys(RandomStringUtils.randomAlphabetic(8));
    }
    public void open (String URL) {
            Driver.getInstance().get(URL);
    }

    public void close () {
        Driver.getInstance().quit();
    }
    public String fourSymb_UserName () {
        String name = RandomStringUtils.randomAlphabetic(4);
        return name;
    }


    public void twentyOneSymb_UserName () {
        userName.click();
        userName.sendKeys(RandomStringUtils.randomAlphabetic(21));
    }

    public void validEmail () {
        email.click();
        email.sendKeys((RandomStringUtils.randomAlphabetic(5) + "@h.com"));
    }

    public String emailWithout_AT_ () {
        String email = (RandomStringUtils.randomAlphabetic(5) + "h.com");
        return email;
    }

    public String passWithoutSpecSymb () {
        String pass = RandomStringUtils.randomAlphabetic(10);
        return pass;
    }

    public void validPass (String pass) {
        password.click();
        password.sendKeys(pass);
    }

    public void confirmPass (String pass) {
        confirmPass.click();
        confirmPass.sendKeys(pass);
    }



    public String generator (char[] arr, String text, int length) {
        String pass = "" + text;
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            pass = pass + arr[random.nextInt(arr.length)];
        }
        return  pass;
    }

    public String nineSpaces () {

        String spaces = "         ";
        return spaces;
    }



}
