package Pages;

import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.Random;

public class AuthPage {

    public AuthPage() {
        PageFactory.initElements(Driver.getInstance(),this);
    }

    @FindBy(css = "")
    private WebElement loginInput;
    @FindBy (css = "")
    private WebElement passwordInput;
    @FindBy (css = "")
    private WebElement inputButton;
    @FindBy(css = "")
    private WebElement userMail;
    @FindBy (xpath = "")
    private WebElement worningMassage;


    public String validEmail_Not_in_BD () {
        String email = (RandomStringUtils.randomAlphabetic(5) + "@h.com");
        return email;
    }

    public String emailWithout_AT_ () {
        String email = (RandomStringUtils.randomAlphabetic(5) + "h.com");
        return email;
    }

    public String passWithoutSpecSymb () {
        String pass = RandomStringUtils.randomAlphabetic(10);
        return pass;
    }

    public String passWithoutUpperCase (char[] arr, int length) {
        String pass = "";
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            pass = pass + arr[random.nextInt(arr.length)];
        }
        return  pass;
    }

    public String nineSpaces () {

        String spaces = "         ";
        return spaces;
    }

}
