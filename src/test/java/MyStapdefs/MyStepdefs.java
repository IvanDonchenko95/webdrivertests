package MyStapdefs;

import Pages.Driver;
import Pages.Inform;
import Pages.RegPage;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class MyStepdefs {
     Inform inform= new Inform();
    char[] upperCase = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
    char[] lowerCase = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    char[] specSymb = {'!', '@', '#', '$', '%', '&', '?', '-', '+', '=', '~'};
    char[] nums = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
    char[] otherLang = {'а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ь', 'ы', 'ъ', 'э', 'ю', 'я'};


    @When("^I open Browser$")
    public void iOpenRegistrationPage() throws InterruptedException {
        WebDriverManager.chromedriver().setup();
        Driver.getInstance().get(inform.getUrl());
    }

    @When("^I enter valid user name$")
    public void iEnterValidUserName() {
    RegPage regPage= new RegPage();
        regPage.validUserName();
    }

    @And("^I enter valid email$")
    public void iEnterValidEmail() {
        RegPage regPage= new RegPage();
        regPage.validEmail();
    }

    @And("^I enter valid password$")
    public void iEnterValidPassword() {
        RegPage regPage= new RegPage();
        regPage.validPass(inform.getPassword());
    }

    @And("^I enter confirm password$")
    public void iEnterConfirmPassword() {
        RegPage regPage= new RegPage();
        regPage.confirmPass(inform.getPassword());
    }

    @Then("^I enter go to auth page$")
    public void iEnterGoToAuthPage() {

    }

    @And("^I close browser$")
    public void iCloseBrowser() throws InterruptedException {
        Driver.getInstance().quit();
    }

    @And("^I click signUp button$")
    public void iClickSignUpButton() throws InterruptedException {
        RegPage regPage= new RegPage();
    regPage.getSignUpButton().click();
        Thread.sleep(5000);
    }

    @Then("^I see warning massage$")
    public void iSeeWarningMassage() {
        RegPage regPage= new RegPage();
        String a = regPage.getErrorMassage().getText();

        if (a.equals("User with such email or name already exist"))
        {
            Assertions.assertEquals("User with such email or name already exist",a);
        } else if (a.equals("Incorrect data, please try again")) {
            Assertions.assertEquals("Incorrect data, please try again",a);
        }

    }

    @When("^I enter already registered user name$")
    public void iEnterAlreadyRegisteredUserName() {
        RegPage regPage= new RegPage();
        regPage.getEmail().sendKeys(inform.getEMail());
    }

    @When("^I enter twenty_one symbols in user name field$")
    public void iEnterTwenty_oneSymbolsInUserNameField() {
        RegPage regPage= new RegPage();
        regPage.twentyOneSymb_UserName();
    }

    @When("^I enter spaces in user name field$")
    public void iEnterSpacesInUserNameField() {
        RegPage regPage= new RegPage();
        regPage.getUserName().sendKeys(regPage.nineSpaces());
    }

    @And("^I enter email without @ in email$")
    public void iEnterEmailWithoutInEmail() {
        RegPage regPage= new RegPage();
        regPage.getEmail().sendKeys(regPage.emailWithout_AT_());
    }

    @When("^I enter four symbols in user name field$")
    public void iEnterFourSymbolsInUserNameField() {
        RegPage regPage= new RegPage();
        regPage.getUserName().sendKeys(regPage.fourSymb_UserName());
    }

    @And("^I enter max symbols email$")
    public void iEnterMaxSymbolsEmail() {
        RegPage regPage= new RegPage();
        regPage.getEmail().sendKeys(regPage.generator(lowerCase,"@.d.com",256));
    }

    @And("^I enter same email$")
    public void iEnterSameEmail() {
        RegPage regPage= new RegPage();
        regPage.getEmail().sendKeys(inform.getEMail());
    }

    @And("^I enter seven symbols password$")
    public void iEnterSevenSymbolsPassword() {
        RegPage regPage= new RegPage();
        regPage.getPassword().sendKeys(regPage.generator(upperCase,"!",6));
    }

    @And("^I enter twenty_one symbols password$")
    public void iEnterTwenty_oneSymbolsPassword() {
        RegPage regPage= new RegPage();
        regPage.getPassword().sendKeys(RandomStringUtils.randomAlphabetic(21));
    }

    @And("^I enter nine spaces symbols in password$")
    public void iEnterEightSpacesSymbolsInPassword() {
        RegPage regPage= new RegPage();
        regPage.getPassword().sendKeys(regPage.nineSpaces());
    }

    @And("^I enter lowercase symbols in password$")
    public void iEnterLowercaseSymbolsInPassword() {
        RegPage regPage= new RegPage();
        regPage.getPassword().sendKeys(regPage.generator(lowerCase,"!",12));
    }

    @And("^I enter password without special symbols$")
    public void iEnterPasswordWithoutSpecialSymbols() {
        RegPage regPage= new RegPage();
        regPage.getPassword().sendKeys(regPage.generator(lowerCase,"X",12));
    }

    @When("^I click on Sign in link$")
    public void iClickOnSignInLink() {
        RegPage regPage= new RegPage();
        regPage.getSignInButton().click();
    }

    @When("^I go to regPage$")
    public void iGoToRegPage() {
        RegPage regPage = new RegPage();
        regPage.open(inform.getUrl());
    }

    @After
    public void closeBrowser() {
        Driver.close();
    }
}
