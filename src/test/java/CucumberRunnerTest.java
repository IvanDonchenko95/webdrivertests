import com.cucumber.listener.Reporter;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import org.testng.annotations.AfterSuite;

import java.io.File;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin= {"pretty","html:Report/cucumber-reports/site/cucumber-pretty",
                "json:Report/cucumber-reports/cucumber-html-reports/cucumber.json","com.cucumber.listener.ExtentCucumberFormatter:Report/html/ExtentReport.html"},
        features = "src/main/java/features",
        strict = true
)

public class CucumberRunnerTest extends AbstractTestNGCucumberTests {

    @AfterSuite
    public void generateReport () {
        ReportingUtils.generaeJVMReport();
    }

    @AfterClass
    public static void setup()
    {
        Reporter.loadXMLConfig(new File("src/test/resources/extent-config.xml"));
//Reporter.setSystemInfo("Test User", System.getProperty("user.name"));
        Reporter.setSystemInfo("User Name", "AJ");
        Reporter.setSystemInfo("Application Name", "Test App ");
        Reporter.setSystemInfo("Operating System Type", System.getProperty("os.name").toString());
        Reporter.setSystemInfo("Environment", "Production");
        Reporter.setTestRunnerOutput("Test Execution Cucumber Report");
    }

}
