$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("MyFeatures.feature");
formatter.feature({
  "comments": [
    {
      "line": 1,
      "value": "# language: en"
    }
  ],
  "line": 2,
  "name": "Registration form",
  "description": "",
  "id": "registration-form",
  "keyword": "Feature"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 6532828100,
  "status": "passed"
});
formatter.scenario({
  "line": 7,
  "name": "registration with valid data",
  "description": "",
  "id": "registration-form;registration-with-valid-data",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 6,
      "name": "@reg"
    },
    {
      "line": 6,
      "name": "@all"
    },
    {
      "line": 6,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 8,
  "name": "I go to regPage",
  "keyword": "When "
});
formatter.step({
  "line": 9,
  "name": "I enter valid user name",
  "keyword": "And "
});
formatter.step({
  "line": 10,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 11,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 12,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 13,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iGoToRegPage()"
});
formatter.result({
  "duration": 217581500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 176022200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 169986800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 154771700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 141939400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5082688100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 29463900,
  "error_message": "org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : You should confirm your email}\n  (Session info: chrome\u003d86.0.4240.111): You should confirm your email\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027WIN-7SL266G4HS6\u0027, ip: \u0027192.168.0.103\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002711.0.1\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 86.0.4240.111, chrome: {chromedriverVersion: 86.0.4240.22 (398b0743353ff..., userDataDir: C:\\Users\\Ivan\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:52242}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: 7282e932e844a0188c82a3a7b8bef07a\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:609)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteWebDriverOptions$RemoteWindow.maximize(RemoteWebDriver.java:837)\r\n\tat Pages.Driver.getInstance(Driver.java:16)\r\n\tat MyStapdefs.MyStepdefs.iCloseBrowser(MyStepdefs.java:62)\r\n\tat ✽.And I close browser(MyFeatures.feature:14)\r\n",
  "status": "failed"
});
formatter.after({
  "duration": 643746700,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5849030100,
  "status": "passed"
});
formatter.scenario({
  "line": 17,
  "name": "registration with empty fields",
  "description": "",
  "id": "registration-form;registration-with-empty-fields",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 16,
      "name": "@reg"
    },
    {
      "line": 16,
      "name": "@all"
    },
    {
      "line": 16,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 18,
  "name": "I go to regPage",
  "keyword": "When "
});
formatter.step({
  "line": 19,
  "name": "I enter valid user name",
  "keyword": "And "
});
formatter.step({
  "line": 20,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 21,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 22,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iGoToRegPage()"
});
formatter.result({
  "duration": 206254900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 168732700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5114059000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 62954200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 635848900,
  "status": "passed"
});
formatter.after({
  "duration": 14000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5820051600,
  "status": "passed"
});
formatter.scenario({
  "line": 25,
  "name": "registration with already registered userName",
  "description": "",
  "id": "registration-form;registration-with-already-registered-username",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 24,
      "name": "@reg"
    },
    {
      "line": 24,
      "name": "@all"
    },
    {
      "line": 24,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 26,
  "name": "I go to regPage",
  "keyword": "When "
});
formatter.step({
  "line": 27,
  "name": "I enter already registered user name",
  "keyword": "And "
});
formatter.step({
  "line": 28,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 29,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 30,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 31,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 32,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 33,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iGoToRegPage()"
});
formatter.result({
  "duration": 211186100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterAlreadyRegisteredUserName()"
});
formatter.result({
  "duration": 123264800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 126623500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 153419300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 145362300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5107099100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 57861400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 628841700,
  "status": "passed"
});
formatter.after({
  "duration": 18000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5880534000,
  "status": "passed"
});
formatter.scenario({
  "line": 38,
  "name": "registration with empty userName field",
  "description": "",
  "id": "registration-form;registration-with-empty-username-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 37,
      "name": "@reg"
    },
    {
      "line": 37,
      "name": "@all"
    },
    {
      "line": 37,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 39,
  "name": "I enter valid email",
  "keyword": "When "
});
formatter.step({
  "line": 40,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 41,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 42,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 43,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 44,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 184148300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 140507100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 140169300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5108116200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 59536800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 621710400,
  "status": "passed"
});
formatter.after({
  "duration": 19000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5793431600,
  "status": "passed"
});
formatter.scenario({
  "line": 48,
  "name": "registration with 4 symbols in userName",
  "description": "",
  "id": "registration-form;registration-with-4-symbols-in-username",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 47,
      "name": "@reg"
    },
    {
      "line": 47,
      "name": "@all"
    },
    {
      "line": 47,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 49,
  "name": "I enter four symbols in user name field",
  "keyword": "When "
});
formatter.step({
  "line": 50,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 51,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 52,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 53,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 54,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 55,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterFourSymbolsInUserNameField()"
});
formatter.result({
  "duration": 101930200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 166276100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 144259500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 142945300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5102741100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 58236000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 644783100,
  "status": "passed"
});
formatter.after({
  "duration": 22100,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5826227700,
  "status": "passed"
});
formatter.scenario({
  "line": 59,
  "name": "registration with 21 symbols in userName",
  "description": "",
  "id": "registration-form;registration-with-21-symbols-in-username",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 58,
      "name": "@reg"
    },
    {
      "line": 58,
      "name": "@all"
    },
    {
      "line": 58,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 60,
  "name": "I enter twenty_one symbols in user name field",
  "keyword": "When "
});
formatter.step({
  "line": 61,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 62,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 63,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 64,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 65,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 66,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterTwenty_oneSymbolsInUserNameField()"
});
formatter.result({
  "duration": 150909400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 173529900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 144015500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 143256800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5084516800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 18313200,
  "error_message": "org.openqa.selenium.UnhandledAlertException: unexpected alert open: {Alert text : You should confirm your email}\n  (Session info: chrome\u003d86.0.4240.111): You should confirm your email\nBuild info: version: \u00273.141.59\u0027, revision: \u0027e82be7d358\u0027, time: \u00272018-11-14T08:17:03\u0027\nSystem info: host: \u0027WIN-7SL266G4HS6\u0027, ip: \u0027192.168.0.103\u0027, os.name: \u0027Windows 10\u0027, os.arch: \u0027amd64\u0027, os.version: \u002710.0\u0027, java.version: \u002711.0.1\u0027\nDriver info: org.openqa.selenium.chrome.ChromeDriver\nCapabilities {acceptInsecureCerts: false, browserName: chrome, browserVersion: 86.0.4240.111, chrome: {chromedriverVersion: 86.0.4240.22 (398b0743353ff..., userDataDir: C:\\Users\\Ivan\\AppData\\Local...}, goog:chromeOptions: {debuggerAddress: localhost:52365}, javascriptEnabled: true, networkConnectionEnabled: false, pageLoadStrategy: normal, platform: WINDOWS, platformName: WINDOWS, proxy: Proxy(), setWindowRect: true, strictFileInteractability: false, timeouts: {implicit: 0, pageLoad: 300000, script: 30000}, unhandledPromptBehavior: dismiss and notify, webauthn:virtualAuthenticators: true}\nSession ID: ca3189c2be14add835bcf99c049a5a2e\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:120)\r\n\tat org.openqa.selenium.remote.http.W3CHttpResponseCodec.decode(W3CHttpResponseCodec.java:49)\r\n\tat org.openqa.selenium.remote.HttpCommandExecutor.execute(HttpCommandExecutor.java:158)\r\n\tat org.openqa.selenium.remote.service.DriverCommandExecutor.execute(DriverCommandExecutor.java:83)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:552)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver.execute(RemoteWebDriver.java:609)\r\n\tat org.openqa.selenium.remote.RemoteWebDriver$RemoteWebDriverOptions$RemoteWindow.maximize(RemoteWebDriver.java:837)\r\n\tat Pages.Driver.getInstance(Driver.java:16)\r\n\tat Pages.RegPage.\u003cinit\u003e(RegPage.java:13)\r\n\tat MyStapdefs.MyStepdefs.iSeeWarningMassage(MyStepdefs.java:74)\r\n\tat ✽.Then I see warning massage(MyFeatures.feature:65)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "status": "skipped"
});
formatter.after({
  "duration": 608536100,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5839233700,
  "status": "passed"
});
formatter.scenario({
  "line": 70,
  "name": "registration with spaces in userName",
  "description": "",
  "id": "registration-form;registration-with-spaces-in-username",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 69,
      "name": "@reg"
    },
    {
      "line": 69,
      "name": "@all"
    },
    {
      "line": 69,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 71,
  "name": "I enter spaces in user name field",
  "keyword": "When "
});
formatter.step({
  "line": 72,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 73,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 74,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 75,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 76,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 77,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterSpacesInUserNameField()"
});
formatter.result({
  "duration": 159273000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 204131600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 162427800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 154955300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5116506500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 58684700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 620654600,
  "status": "passed"
});
formatter.after({
  "duration": 9800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5785157200,
  "status": "passed"
});
formatter.scenario({
  "line": 81,
  "name": "registration with empty Email field",
  "description": "",
  "id": "registration-form;registration-with-empty-email-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 80,
      "name": "@reg"
    },
    {
      "line": 80,
      "name": "@all"
    },
    {
      "line": 80,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 82,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 83,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 84,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 85,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 86,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 87,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 170392000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 139368400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 141406800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5107341200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 61212300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 653012500,
  "status": "passed"
});
formatter.after({
  "duration": 11200,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5877545100,
  "status": "passed"
});
formatter.scenario({
  "line": 91,
  "name": "registration without @ in Email",
  "description": "",
  "id": "registration-form;registration-without-@-in-email",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 90,
      "name": "@reg"
    },
    {
      "line": 90,
      "name": "@all"
    },
    {
      "line": 90,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 92,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 93,
  "name": "I enter email without @ in email",
  "keyword": "And "
});
formatter.step({
  "line": 94,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 95,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 96,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 97,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 98,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 187572500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterEmailWithoutInEmail()"
});
formatter.result({
  "duration": 122231500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 154903400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 160817100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5100966600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 64411800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 670090000,
  "status": "passed"
});
formatter.after({
  "duration": 12800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5910468700,
  "status": "passed"
});
formatter.scenario({
  "line": 103,
  "name": "registration with 256 symbols in Email",
  "description": "",
  "id": "registration-form;registration-with-256-symbols-in-email",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 102,
      "name": "@reg"
    },
    {
      "line": 102,
      "name": "@all"
    },
    {
      "line": 102,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 104,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 105,
  "name": "I enter max symbols email",
  "keyword": "And "
});
formatter.step({
  "line": 106,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 107,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 108,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 109,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 110,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 173577200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterMaxSymbolsEmail()"
});
formatter.result({
  "duration": 311576800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 169250800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 129246400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5098183900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 57629700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 1233775200,
  "status": "passed"
});
formatter.after({
  "duration": 20300,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5849436500,
  "status": "passed"
});
formatter.scenario({
  "line": 113,
  "name": "registration with already registered Email",
  "description": "",
  "id": "registration-form;registration-with-already-registered-email",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 112,
      "name": "@reg"
    },
    {
      "line": 112,
      "name": "@all"
    },
    {
      "line": 112,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 114,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 115,
  "name": "I enter same email",
  "keyword": "And "
});
formatter.step({
  "line": 116,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 117,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 118,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 119,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 120,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 144602600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterSameEmail()"
});
formatter.result({
  "duration": 93743700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 124906300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 143190300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5081371700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 64037000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 633821700,
  "status": "passed"
});
formatter.after({
  "duration": 12200,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5883636500,
  "status": "passed"
});
formatter.scenario({
  "line": 123,
  "name": "registration with empty Password field",
  "description": "",
  "id": "registration-form;registration-with-empty-password-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 122,
      "name": "@reg"
    },
    {
      "line": 122,
      "name": "@all"
    },
    {
      "line": 122,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 124,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 125,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 126,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 127,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 128,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 129,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 176143300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 183295200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 138735500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5098274400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 65695500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 637899500,
  "status": "passed"
});
formatter.after({
  "duration": 11000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5835863500,
  "status": "passed"
});
formatter.scenario({
  "line": 133,
  "name": "registration with 7 symbols in Password",
  "description": "",
  "id": "registration-form;registration-with-7-symbols-in-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 132,
      "name": "@reg"
    },
    {
      "line": 132,
      "name": "@all"
    },
    {
      "line": 132,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 134,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 135,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 136,
  "name": "I enter seven symbols password",
  "keyword": "And "
});
formatter.step({
  "line": 137,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 138,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 139,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 140,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 149290800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 191082600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterSevenSymbolsPassword()"
});
formatter.result({
  "duration": 107483200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 143281700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5106979600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 61027700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 655191700,
  "status": "passed"
});
formatter.after({
  "duration": 63600,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5852161800,
  "status": "passed"
});
formatter.scenario({
  "line": 144,
  "name": "registration with 21 symbols in Password",
  "description": "",
  "id": "registration-form;registration-with-21-symbols-in-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 143,
      "name": "@reg"
    },
    {
      "line": 143,
      "name": "@all"
    },
    {
      "line": 143,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 145,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 146,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 147,
  "name": "I enter twenty_one symbols password",
  "keyword": "And "
});
formatter.step({
  "line": 148,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 149,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 150,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 151,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 149604500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 168585200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterTwenty_oneSymbolsPassword()"
});
formatter.result({
  "duration": 101206100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 156300600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5121474000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 62998200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 643539300,
  "status": "passed"
});
formatter.after({
  "duration": 19400,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5839892700,
  "status": "passed"
});
formatter.scenario({
  "line": 155,
  "name": "registration with spaces symbols in Password",
  "description": "",
  "id": "registration-form;registration-with-spaces-symbols-in-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 154,
      "name": "@reg"
    },
    {
      "line": 154,
      "name": "@all"
    },
    {
      "line": 154,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 156,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 157,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 158,
  "name": "I enter nine spaces symbols in password",
  "keyword": "And "
});
formatter.step({
  "line": 159,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 160,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 161,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 162,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 152902900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 174261500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterEightSpacesSymbolsInPassword()"
});
formatter.result({
  "duration": 105751800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 138564700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5107616900,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 59438000,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 642401300,
  "status": "passed"
});
formatter.after({
  "duration": 12800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5883417300,
  "status": "passed"
});
formatter.scenario({
  "line": 166,
  "name": "registration with lowercase in Password",
  "description": "",
  "id": "registration-form;registration-with-lowercase-in-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 165,
      "name": "@reg"
    },
    {
      "line": 165,
      "name": "@all"
    },
    {
      "line": 165,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 167,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 168,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 169,
  "name": "I enter lowercase symbols in password",
  "keyword": "And "
});
formatter.step({
  "line": 170,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 171,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 172,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 173,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 149711800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 156669300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterLowercaseSymbolsInPassword()"
});
formatter.result({
  "duration": 94055500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 141787100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5100279800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 62180200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 655497200,
  "status": "passed"
});
formatter.after({
  "duration": 580800,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5862763000,
  "status": "passed"
});
formatter.scenario({
  "line": 177,
  "name": "registration without special symbols in Password",
  "description": "",
  "id": "registration-form;registration-without-special-symbols-in-password",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 176,
      "name": "@reg"
    },
    {
      "line": 176,
      "name": "@all"
    },
    {
      "line": 176,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 178,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 179,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 180,
  "name": "I enter password without special symbols",
  "keyword": "And "
});
formatter.step({
  "line": 181,
  "name": "I enter confirm password",
  "keyword": "And "
});
formatter.step({
  "line": 182,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 183,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 184,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 174461600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 169066500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterPasswordWithoutSpecialSymbols()"
});
formatter.result({
  "duration": 95828800,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterConfirmPassword()"
});
formatter.result({
  "duration": 142396600,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5123548100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 60714500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 638314800,
  "status": "passed"
});
formatter.after({
  "duration": 52000,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5791034300,
  "status": "passed"
});
formatter.scenario({
  "line": 188,
  "name": "registration with empty Confirm Password field",
  "description": "",
  "id": "registration-form;registration-with-empty-confirm-password-field",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 187,
      "name": "@reg"
    },
    {
      "line": 187,
      "name": "@all"
    },
    {
      "line": 187,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 189,
  "name": "I enter valid user name",
  "keyword": "When "
});
formatter.step({
  "line": 190,
  "name": "I enter valid email",
  "keyword": "And "
});
formatter.step({
  "line": 191,
  "name": "I enter valid password",
  "keyword": "And "
});
formatter.step({
  "line": 192,
  "name": "I click signUp button",
  "keyword": "And "
});
formatter.step({
  "line": 193,
  "name": "I see warning massage",
  "keyword": "Then "
});
formatter.step({
  "line": 194,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iEnterValidUserName()"
});
formatter.result({
  "duration": 147639200,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidEmail()"
});
formatter.result({
  "duration": 164665300,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterValidPassword()"
});
formatter.result({
  "duration": 150253500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iClickSignUpButton()"
});
formatter.result({
  "duration": 5103378100,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iSeeWarningMassage()"
});
formatter.result({
  "duration": 59837400,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 656220100,
  "status": "passed"
});
formatter.after({
  "duration": 10200,
  "status": "passed"
});
formatter.background({
  "line": 3,
  "name": "",
  "description": "",
  "type": "background",
  "keyword": "Background"
});
formatter.step({
  "line": 4,
  "name": "I open Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "MyStepdefs.iOpenRegistrationPage()"
});
formatter.result({
  "duration": 5848555500,
  "status": "passed"
});
formatter.scenario({
  "line": 198,
  "name": "redirect to authorization page",
  "description": "",
  "id": "registration-form;redirect-to-authorization-page",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 197,
      "name": "@reg"
    },
    {
      "line": 197,
      "name": "@all"
    },
    {
      "line": 197,
      "name": "@smoke"
    }
  ]
});
formatter.step({
  "line": 199,
  "name": "I click on Sign in link",
  "keyword": "When "
});
formatter.step({
  "line": 200,
  "name": "I enter go to auth page",
  "keyword": "Then "
});
formatter.step({
  "line": 201,
  "name": "I close browser",
  "keyword": "And "
});
formatter.match({
  "location": "MyStepdefs.iClickOnSignInLink()"
});
formatter.result({
  "duration": 446171500,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iEnterGoToAuthPage()"
});
formatter.result({
  "duration": 24700,
  "status": "passed"
});
formatter.match({
  "location": "MyStepdefs.iCloseBrowser()"
});
formatter.result({
  "duration": 648869600,
  "status": "passed"
});
formatter.after({
  "duration": 9000,
  "status": "passed"
});
});